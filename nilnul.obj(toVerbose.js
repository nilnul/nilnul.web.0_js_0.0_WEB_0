﻿(function (nilnul) {
	nilnul = nilnul || {};

	Object.prototype.toString = function () {
		var s = "{\n";
		for (var i in this) {
			s +="\t" +i.toString()+":"+this[i].toString()+",\n";
		}
		s += "}";
		return s;
	};


} )(nilnul);