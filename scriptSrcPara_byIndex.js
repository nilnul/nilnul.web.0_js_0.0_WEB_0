﻿var scripts = document.getElementsByTagName('script');
var index = scripts.length - 1;
var myScript = scripts[index];
// myScript now contains our script object
var queryString = myScript.src.replace(/^[^\?]+\??/, '');



/*


"...since scripts in HTML (not XHTML) are executed as loaded..." Note that the defer and async attributes modify that behavior, so if you're using them, this may not work: Your script may not be the last in the series at that point. Relying on that would give me the shivers anyway, so I'd probably use the filename and filter on src (granted that introduces a restriction that the file can't be renamed). –  T.J. Crowder Feb 15 '12 at 14:08  
 

1     
 
Filtering on src might be tricky if you referenced the same script multiple times with different params. –   


*/