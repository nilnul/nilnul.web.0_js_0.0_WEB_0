﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CdnAndFallBack.ascx.cs" Inherits="nilnul.web.js._web._ascxRender.CdnAndFallBack"  %>
<%@ OutputCache Duration="10000" VaryByParam="*" %>
<%@ Register Src="~/jQuery/AspNetCdn.ascx" TagPrefix="uc1" TagName="AspNetCdn" %>

<uc1:AspNetCdn runat="server" id="AspNetCdn" />
<script>
	// Fallback to loading jQuery from a local path if the CDN is unavailable
	(window.jQuery || document.write('<%=this.str %>'));
</script>
