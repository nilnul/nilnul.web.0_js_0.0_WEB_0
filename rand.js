﻿/// <reference path="nilnul2.js" />



(function (window, nilnul, undefined) {


	nilnul.rand = function (x, y) {
		return x + Math.random(y);
	};



	nilnul.randInt = function (x, y) {
		return Math.floor(rand(x, y));
	};

	nilnul.randChar = function (s) {
		return s[randint(0, s.length)];
	};



	var alphabet = function (_symbol) {
		return function () {
			var $symbol =
			{
				symbol: function (){ return _symbol; }
			,
				length: function (){ return _symbol.length; }
				,
				last: function (){ return this.symbol()[length - 1]; }
				,
				isLast: function (char){ return this.last() == char; }
				,
				first: function (){ return this.symbol()[0]; }
				,
				index: function (char){ return this.symbol().indexOf(char); }


			};

			$symbol.randChar = function () {

				return _symbol[nilnul.randInt(0, _symbol.length)];

			};

			return $symbol;
		}();

	}

	nilnul.alphabet = {};
	nilnul.alphabet.prefix = alphabet("abcdefghijklmnopqrstuvwxyz");
	nilnul.alphabet.suffix = alphabet("abcdefghijklmnopqrstuvwxyz_0123456789");
	nilnul.alphabet.suffix2 = alphabet("0123456789_abcdefghijklmnopqrstuvwxyz");




	nilnul.getElementIds = function () {
		//get all elements ids

		var arr = [];
		var elem = document.getElementsByTagName('*'), i = 0, e;
		var id = "id";
		while (e = elem[i++]) {
			e[id] ? arr[arr.length] = e[id] : null;
		}
		return arr;

	};


	if (typeof String.prototype.startsWith != 'function') {
		String.prototype.startsWith = function (str) {
			return this.indexOf(str) == 0;
		};
	}


	var nextChar = function (char, symbol) {

		return symbol[symbol.indexOf(char) + 1];
	};
	var nextPrefix = function (char) {
		return nextChar(char, nilnul.alphabet.prefix.symbol());

	};

	var nextSuffix = function (char) {

		return nextChar(char, nilnul.alphabet.suffix.symbol());

	};

	var nextCharBool = function (char, prefix) {
		if (prefix) {

			return nextPrefix(char);
		}
		return nextSuffix(char);

	}

	var nextCharNatural = function (char, prefix) {
		if (prefix == 0) {

			return nextPrefix(char);
		}
		return nextSuffix(char);

	}



	nilnul.genId = function () {


		var incrementId = function (id) {

			if (id.length == 0) {
				return nilnul.alphabet.prefix.symbol().first();
			}

			if (id.length == 1) {
				if (id[id.length] == nilnul.alphabet.prefix.last()) {
					return id + nilnul.alphabet.suffix.first();
				}
				return  nextChar(id, nilnul.alphabet.prefix.symbol());

			}

			if (id[id.length - 1] == nilnul.alphabet.suffix.last()) {
				return id + nilnul.alphabet.suffix.first();
			}
			return id.substr(0, id.length - 1) + nextChar(id[id.length - 1], nilnul.alphabet.suffix.symbol());
			//return id;


		};

		return {

			increId: function (curId, nextId) {
				if (nilnul.alphabet.suffix.index(curId[curId.length - 1]) <= nilnul.alphabet.suffix.index(nextId[curId.length - 1])) {

					return incrementId(curId);

				}


			},

			increIdByChar: function (curId){ }
		}
	}();

	var genRndSeg = function (len, symbol) {
		var r="";

		while (r.len < len) {
			r=r+symbol.randChar();
 
		}

		return r;

	};


	//given a prefix, generate a string of certain lenght, loop, and append generated string if necessary.
	nilnul.newId = function (prefix) {

		var ids = nilnul.getElementIds();
		var segLen=6;
		var symbol=nilnul.alphabet.suffix2;	
		var genRndSeg=function(){
			return genRndSeg(segLen,symbol);
		};

		var newId = prefix + genRndSeg();
	

		var incre = function (id) {

			//if (id.length == 0) {
			//	return nilnul.alphabet.prefix.symbol().first();
			//}

			//if (id.length == 1) {
			//	if (id[id.length] == nilnul.alphabet.prefix.last()) {
			//		return id + nilnul.alphabet.suffix.first();
			//	}
			//	return  nextChar(id, nilnul.alphabet.prefix.symbol());

			//}

			if (id[id.length - 1] == nilnul.alphabet.suffix.last()) {
				return id + nilnul.alphabet.suffix.first();
			}
			return id.substr(0, id.length - 1) + nextChar(id[id.length - 1], nilnul.alphabet.suffix.symbol());
			//return id;


		};

		var crawl = function (curId, hurdle) {
			var curIdLast = curId[curId.length - 1];
			var hurdleInPos = hurdle[curId.length - 1];

			while (hurdle.startsWith(curId)) {
				curId=curId+genRndSeg();
 
			}
			return curId;
		}

		var crawlAll = function (curId, hurdles) {
			
			for (var i = 0; i < hurdles.length; i++) {
				
				curId=crawl(curId,hurdles[i];
			}
			return curId;
		};




		return newId;
	};




})(
	window, nilnul, undefined
);
