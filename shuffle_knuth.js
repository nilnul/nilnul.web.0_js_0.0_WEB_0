﻿function knuth_shuffle(a) {
	var n = a.length,
        r,
        temp;
	while (n > 1) {
		r = Math.floor(n * Math.random());
		n -= 1;
		temp = a[n];
		a[n] = a[r];
		a[r] = temp;
	}
	return a;
}