﻿<%@ Control Language="C#" AutoEventWireup="true"  %>

<%@ Register Src="~/JQuery1_10_2min.ascx" TagPrefix="uc1" TagName="JQuery1_10_2min" %>
<%@ Register Src="~/jQuery/AspNetCdn.ascx" TagPrefix="uc1" TagName="AspNetCdn" %>

<uc1:AspNetCdn runat="server" id="AspNetCdn1" />


<script>
	// Fallback to loading jQuery from a local path if the CDN is unavailable
	(window.jQuery || document.write('<script src="<%=nilnul.web.js._web.Cfg.PATH_SLASH+"jquery-1.10.2.min.js"%>"><\/script>'));
</script>
