﻿//<script type="text/javascript">
function getCurrentScriptPath() {
	//firefox可以直接得到currentScript
	if (document.hasOwnProperty('currentScript')) {
		return document.currentScript.src;
	}
	var scripts = document.scripts || document.getElementsByTagName('script'),
	  len = scripts.length,
	  state = 'interactive';
	//兼容IE
	for (var i=0; i <len; i++) {
		if (scripts[i].readyState==state) {
			return scripts[i].src;
		}
	}
	//webkit
	return scripts[len-1].src;
}
//</script>
