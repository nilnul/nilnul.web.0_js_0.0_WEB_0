﻿<%@ Control Language="C#" AutoEventWireup="true"  %>

<% #if false %>
<script>
	<% #endif %>

	<%--
	 have the following to snippets of code:

$(document).ready(function() {
    document.head.appendChild(
        $('<script />').attr('src', 'source.js').on('load', function() {
            ...
        })[0]
    );
});
This will fire the load handler.

Whereas using the normal jQuery append():

$(document).ready(function() {
   $('head').append(
        $('<script />').attr('src', 'source.js').on('load', function() {
            ...
        })
    );
});
This will not fire the load hander.

What am I missing: why does jQuery append() not work?

Is using document.head.appendChild() a bad idea?

NOTE: I can't use $.getScript(). The code will run on a local file system and chrome throws cross site script errors.


--------------------------------------------------------------------------------

Update

Some people had trouble reading the compact style, so I used extra line feeds to clarify which objects where calling which methods. I also made it explicit that my code is inside a $(document).ready block.


--------------------------------------------------------------------------------

Solution

In the end I went with:

$(document).ready(function() {
    $('head')[0].appendChild(
        $('<script />').attr('src', 'source.js').on('load', function() {
            …
        })[0]
    );
});
I think @istos was right in that something in domManip is breaking load.


	jQuery is doing some funny business in its DOM manipulation code. If you look at jQuery's source, you'll see that it uses a method called domManip() inside the append() method.

This domManip() method creates a document fragment (it looks like the node is first appended to a "safe" fragment) and has a lot of checks and conditions regarding scripts. I'm not sure why it uses a document fragment or why all the checks about scripts exist but using the native appendChild() instead of jQuery's append() method fires the event successfully. Here is the code:

Live JSBin: http://jsbin.com/qubuyariba/1/edit


	--%>
	var url = 'http://d3js.org/d3.v3.min.js';
	var s = document.createElement('script');
	s.src = url;
	s.async = true;

	$(s).on('load', function (e) {
		console.log(!!window.d3); // d3 exists
		$(document.body).append('<h1>Load fired!</h1>');
	});

	$('head').get(0).appendChild(s);

	<%-- 
	
	Update:
		appendChild() is a well supported method and there is absolutely no reason not to use it in this case.

	--%>



	<% #if false %>
</script>
<% #endif %>
