﻿(function (nilnul,window) {
	nilnul = nilnul || {};
	nilnul.print = nilnul.print || function () { };

	var insideSection = function (section, prefix, counter, infix,suffix) {

		var $h1 = $("h1:first-of-type", section);
		var h1 = $h1[0];

		var newPrefix = prefix + counter + infix;
		//if (suffix===' ') {
		//	suffix = '&nbsp;';
		//}

		var size = 1+  (9- prefix.split(infix).length)/18;

		$h1.css("font-size",size.toString()+ "em");

		h1.insertBefore(document.createTextNode(prefix + counter + infix+suffix), h1.firstChild);

		var subs =	Array.prototype.filter.call(
			section.childNodes
			,
			function (e, i, c) {
				return e.nodeName === "SECTION"
			}

		);
		for (var i = 0; i < subs.length; i++) {

			insideSection(subs[i], newPrefix, i + 1, infix,suffix)

		}


	};

	var findTopSection = function (container, initial, prefix, infix,suffix) {
		container = container || window.document.body;
		initial = initial || 0;
		prefix = prefix || "";
		infix = infix || ".";
		suffix = suffix || " ";

		var $section = $("section:first", container);


		if ($section.length > 0) {
			var section = $section[0];
			var subs = Array.prototype.filter.call(
				section.parentElement.childNodes
				,
				function (e, i, c) {
					return e.nodeName === "SECTION"
				}

			);
			for (var i = 0; i < subs.length; i++) {

				insideSection(subs[i], prefix, i + initial, infix,suffix)
			}


		}

	};

	nilnul.print.number = findTopSection;

})(
	nilnul,window
);
