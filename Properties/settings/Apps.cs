﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using nilnul.web.ascxRender.handler;
using nilnul.web.Properties.settings._url;

namespace nilnul.web._js_._WEB_.Properties.settings
{
	public class App : nilnul.web.Properties.settings._url.origin.app.co_.ProdDebug
	{
		public App() : base(Settings.Default.denote)
		{
		}

		

		static public App Singleton
		{
			get
			{
				return nilnul.obj_.Singleton<App>.Instance;
			}
		}

	}
}