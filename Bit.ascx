﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<% #if false %>
<script>
	<% #endif %>

	(function (nilnul) {
		nilnul.bit = function () { };

		nilnul.bit.vow_ = function () { };
		nilnul.bit.vow_.True = function (b) {
			if (!b) {
				throw new nilnul.obj._vow.Xpn(b + " is not true!");
			}
		};
		nilnul.bit.vow_.False = function (x) {
			if (b) {
				throw new nilnul.obj._vow.Xpn(b + " is not false!");
			}

		}
			;
		nilnul.bit.op_ = function () { };
		nilnul.bit.op_.unary_ = function () { };

		nilnul.bit.op_.binary_ = function () { };
		nilnul.bit.op_.binary_.imply = function (x, y) {
			return !x || y;
		};

		nilnul.bit.op_.binary_.ne = function (x, y) {
			return !(x === y);
		};

		nilnul.bit.op_.binary_.ge = function (x, y) {
			return x || !y;
		};
		nilnul.bit.op_.binary_.nand = function (x, y) {
			return !(x && y);
		};
		nilnul.bit.op_.binary_.and = function (x, y) { return x && y; };
		nilnul.bit.op_.binary_.and.cumulator = {
			ini: true
			,
			op: function (x,y) {
				return x && y;
			}
			,
			cumulate: function (bits) {
				return bits.reduce(this.op, this.ini);
			}
		};
		nilnul.bit.op_.binary_.or = function (x, y) { return x || y; };
		nilnul.bit.op_.binary_.or.cumulator = {
			ini: true
			,
			op: nilnul.bit.op_.binary_.or
			,
			cumulate: function (bits) {
				return bits.reduce(this.op, this.ini);
			}
		};


	



	})(
		nilnul
	);
	<% #if false %>
</script>
<% #endif %>

