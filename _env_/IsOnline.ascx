﻿<%@ Control Language="C#" AutoEventWireup="true" %>

<% #if false %>
<script>
	<% #endif %>

	// We'll assume we aren't online.
	var online = false;

	// Assume we're a packaged app with systemXHR permissions.
	// https://developer.mozilla.org/docs/Web/Apps/App_permissions
	var request = new window.XMLHttpRequest({ mozSystem: true });
	request.open('HEAD', 'http://www.mozilla.org/robots.txt', true);
	request.timeout = 5750;

	request.addEventListener('load', function (event) {
		console.log('We seem to be online!', event);
		online = true;
	});

	var offlineAlert = function (event) {
		console.log('We are likely offline:', event);
	}

	request.addEventListener('error', offlineAlert);
	request.addEventListener('timeout', offlineAlert);

	request.send(null);

	<% #if false %>
</script>
<% #endif %>
