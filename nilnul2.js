﻿(
	function (window, undefined) {


		var _nilnul = {
			trim: String.prototype.trim,
			trim_regex: function (s) { return s.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); },
			emailRegex: /^(?:[-_a-zA-Z0-9]+\.)*(?:[-_a-zA-Z0-9]+)@(?:[-_a-zA-Z0-9]+\.)+[-_a-zA-Z0-9]+$/


		};



		_nilnul.appendEvents = function (target, evt, func) {

			var oldEvt = target[evt];
			if (typeof oldEvt != 'function') {
				target[evt] = func;
			} else {
				target[evt] = function (e) {
					oldEvt(e);
					func(e);
				}
			}
		};





		_nilnul.isEmail = function (s) {
			return window.nilnulEs.emailRegex.test(s);

		};

		_nilnul.userLang = (window.navigator.language) ? window.navigator.language : window.navigator.userLanguage;



		_nilnul.getUserLang = function () {
			var s = window.nilnulEs.userLang;
			if (s.indexOf("cn") >= 0 || s.indexOf("zh") != -1) {
				return "cn";

			}
		}
		_nilnul.fireAction = function (action) {
			if (action && typeof (action) == "function") {
				action();
			}
		};

		_nilnul.loadControl = function () {


		}

		//	nilnulJs.trim = String.prototype.trim;

		//nilnulJs.trim_regex = function (){ return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); };

		//window.nilnulJs.isNullOrWhiteSpace = function (s) {
		//	if (s == null) {
		//		return true;

		//	}
		//	if (typeof(s) == "undefined") {
		//		return null;

		//	}
		//	if (s.trim() == "") {
		//		return true;

		//	}
		//	return false;


		//}

		_nilnul.isNullOrWhiteSpace = function (s) {
			if (typeof (s) == "undefined") {
				return true;

			}

			if (s == null) {
				return true;

			}
			if (typeof s == "string" && s.trim() == "") {
				return true;

			}
			return false;


		}


		_nilnul.isNoteNullOrWhiteSpace = function (s) {
			return !window.isNullOrWhiteSpace(s);

		}

		_nilnul.emailRegex = /^(?:[-_a-zA-Z0-9]+\.)*(?:[-_a-zA-Z0-9]+)@(?:[-_a-zA-Z0-9]+\.)+[-_a-zA-Z0-9]+$/;

		_nilnul.setCookie = function (c_name, value, exdays) {
			var exdate = new Date();
			exdate.setDate(exdate.getDate() + exdays);

			var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
			document.cookie = c_name + "=" + c_value;
		}

		_nilnul.getCookie = function (c_name) {
			var i, x, y, ARRcookies = document.cookie.split(";");

			for (i = 0; i < ARRcookies.length; i++) {
				x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));

				x = x.trim();
				//x.replace(/^\s+|\s+$/g, "");
				if (x == c_name.trim) {
					y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
					return unescape(y);
				}
			}
		}

		window.nilnulEs = window.nilnulJs = window.nilnul = _nilnul;

		_nilnul.userKeyInCookie = "nilnulUserKeyInCookie";

		_nilnul.getUser = function () {
			return getCookie(window.userKeyInCookie);
		}

		_nilnul.queryString = function (key) {
			qs = window.location.search.substring(1);

			qa = qs.split("&");

			for (i = 0; i < qa.length; i++) {
				keyValue = qa[i].split("=");
				if (keyValue[0] == key) {
					return keyValue[1];
				}
			}
		}

		_nilnul.getHttp = function () {
			var xmlhttp = false;
			if (window.XMLHttpRequest) {
				xmlhttp = new XMLHttpRequest();
				if (xmlhttp.overrideMimeType) {
					xmlhttp.overrideMimeType('text/xml');
				}
			} else {
				try {
					xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
					try {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (E) {
						try {

							xmlhttp = new ActiveXObject("Msxml2.XMLHTTP.6.0");
						} catch (e2) {

							try { xmlhttp = new ActiveXObject("Msxml2.XMLHTTP.3.0"); }
							catch (e4) {
								xmlhttp = false;
							}
						}
					}
				}
			}
			return xmlhttp;
		}


		_nilnul.loadCss = function (cssPath) {

			if (document.createStyleSheet) { document.createStyleSheet(cssPath); }
			else {
				$("head").append($("<link rel='stylesheet' href='" + cssPath + "' type='text/css' media='screen' />"));
			}

		};



	}

)
(
	window, undefined
);

(function (window, undefined) {
	String.isNullOrWhiteSpace = function () {

		if (typeof (s) == "undefined") {
			return true;

		}

		if (s == null) {
			return true;

		}
		if (typeof s == "string" && s.trim() == "") {
			return true;

		}
		return false;

	};


	String.format = function (text) {
		//check if there are two arguments in the arguments list
		if (arguments.length <= 1) {
			//if there are not 2 or more arguments there's nothing to replace
			//just return the original text
			return text;
		}
		//decrement to move to the second argument in the array
		var tokenCount = arguments.length - 1;
		for (var token = 0; token < tokenCount; token++) {
			//iterate through the tokens and replace their
			// placeholders from the original text in order
			text = text.replace(
				new RegExp("\\{" + token + "\\}", "gi"),
				arguments[token + 1]
			);
		}
		return text;
	};

	var color = function (r, g, b, a) {
		return String.format("rgba({0},{1},{2},{3})", r, g, b, a);
	};
	nilnul.rgba = color;


	//array.fileter

	if (!Array.prototype.filter) {
		Array.prototype.filter = function (fun /*, thisp */) {
			"use strict";
			if (this == null) throw new TypeError();
			var t = Object(this);
			var len = t.length >>> 0;
			if (typeof fun != "function") throw new TypeError();
			var res = [];
			var thisp = arguments[1];
			for (var i = 0; i < len; i++) {
				if (i in t) {
					var val = t[i]; // in case fun mutates this    
					if (fun.call(thisp, val, i, t)) res.push(val);
				}
			} return res;
		};
	}




})(

window, undefined


);

(function (window, undefined, nilnul) {

	nilnul.css = nilnul.css || {};
	nilnul.css.classes = nilnul.css.classes || {};
	nilnul.css.classes.has = function (f) {
		var hasStyle = false;
		var fullStylesheets = document.styleSheets;
		for (var sx = 0; sx < fullStylesheets.length; sx++) {
			var sheetclasses = fullStylesheets[sx].rules || document.styleSheets[sx].cssRules;
			for (var cx = 0; cx < sheetclasses.length; cx++) {
				if (sheetclasses[cx].selectorText == f) {
					hasStyle = true;
					break;
					//return classes[x].style;               
				}
			}
		}
		return hasStyle;
	};



}(window, void null, nilnul));


