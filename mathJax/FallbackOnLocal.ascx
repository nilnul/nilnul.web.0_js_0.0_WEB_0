﻿<%@ Control Language="C#" AutoEventWireup="true"  %>
<script src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">

</script>
<script>

	// Fallback to loading jQuery from a local path if the CDN is unavailable
	(window.MathJax || document.write('<script src="http://nilnul.com/app_/nilnul.web._js_/mathJax2/MathJax.js?config=TeX-AMS-MML_HTMLorMML"><\/script>'));
 </script>
