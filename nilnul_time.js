﻿///<reference path="nilnul.measure.js" />
(function (nilnul)
{
	var time = function ()
	{
	};

	var SECOND_SPAN=1000;	//in milliseconds
	var MINUTE_SPAN=60*SECOND_SPAN;
	var HOUR_SPAN=60*MINUTE_SPAN;
	var DAY_SPAN=24*HOUR_SPAN;
	var MONTH_SPAN=DAY_SPAN*30;
	var YEAR_SPAN=MONTH_SPAN*12;

	var daySpan = 86400000;	//InMilliSeconds
	var monthSpan = 30 * daySpan;
	var yearSpan = daySpan * 365;	//(new Date(1901,0).getTime() - new Date(1900,0).getTime());

	time.unit = {};


	time.determineUnit = function (timeSpan)	//in milliSeconds
	{

		///in milliseconds.
		//var now = new Date();
		//var yearFromNow = new Date();
		//yearFromNow.setFullYear(now.getFullYear() + 1);
		//var yearSpan = yearFromNow - now;
		////	var monthFromNow=new 
		//var monthSpan = (new Date(1901, 0).getTime() - new Date(1900, 0).getTime());

		var yearsWeight = 10;
		if (timeSpan >= yearSpan)
		{
			return determineYear(timeSpan);


		} else if (timeSpan >= monthSpan)
		{
			return new nilnul.measure("month");

		} else if (timeSpan >= daySpan)
		{

			return new nilnul.measure("day");
		
		}
		else if (timeSpan >= HOUR_SPAN)
		{
			return new nilnul.measure("hour");
		
		}
		else if (timeSpan >= MINUTE_SPAN)
		{
			return new nilnul.measure("minute");
		
		}
		else 
		{
			return determineSeconds(timeSpan);

		}

		var _determineSplitSeconds = function (splitSeconds)
		{
			var quantity = .1;

			var currentScale = quantity * SECOND_SPAN;

			var unit = { unit: "splitSeconds", quantity: .1 };

			while (splitSeconds < currentScale)
			{
				currentScale = quantity * SECOND_SPAN;

			}

			return new nilnul.measure("splitSeconds",currentScale);

		};


		var determineSeconds = function (seconds)	///less than 1 minute
		{
			var quantity = 1;

			//var currentScale = quantity * SECOND_SPAN;

			//var unit = { unit: "splitSeconds", quantity: .1 };

			while (seconds < quantity*SECOND_SPAN)
			{
				quantity /= 10;
				//currentScale = quantity * SECOND_SPAN;

			}

			return new nilnul.measure("seconds", quantity);

		};


		var determineYear = function (timeSpan)
		{

			//loop to find the magnitude
			var quantity = 1;
			var yearsSpan = quantity * yearSpan;
			var unit = { unit: "year", quantity: 1 };

			while (timeSpan / yearsSpan >= 10)
			{
				quantity *= 10;
				yearsSpan = quantity * yearSpan;
			}
			return { quantity: quantity, unit: "year" };
		};


		if (typeof (nilnul) == "undefined")
		{
			nilnul = {};
		};

		nilnul.time= time;



	};






})(

nilnul



);