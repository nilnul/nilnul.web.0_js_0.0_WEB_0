﻿///<reference path="nilnul.measure.js" />
(function (nilnul) {
	nilnul.cookie = function () { };

	nilnul.cookie.set = function (name, value, days, domain, path) {
		var exdays = days;
		var c_name = name;

		var s = name + "=" + escape(value);

		var expiresTime;


		if (days) {

			if (days === Number.POSITIVE_INFINITY) {
				//expiresTime = "Fri, 31 Dec 9999 23:59:59 GMT";

				expiresTime = "Tue, 19 Jan 2038 03:14:07 GMT";


			} else {

				var exdate = new Date();

				exdate.setDate(exdate.getDate() + days);
				expiresTime = exdate.toUTCString();


			}



			s += "; expires=" + expiresTime;

		} 
		if (domain) {
			s += ";domain=" + domain;
		}
		if (path) {
			s += ";path=" + path;
		}


		document.cookie = s;
	};
	nilnul.cookie.set.inf = function (key, val) {
		nilnul.cookie.set(key, val, Number.POSITIVE_INFINITY);

	};

	nilnul.cookie.del = function (name, path, domain) {

		document.cookie =
			name
			+
			"="
			+
			"; expires=Thu, 01-Jan-1970 00:00:01 GMT"
			+
			(
				(path) ? "; path=" + path : ""
			)
				+
			(
					(domain) ? "; domain=" + domain : ""
			)

		;

		//nilnul.cookie.set(name, "", -1);
	};

	nilnul.cookie.get = function (c_name) {
		var i, x, y, ARRcookies = document.cookie.split(";");

		for (i = 0; i < ARRcookies.length; i++) {
			x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));

			x = x.trim();
			//x.replace(/^\s+|\s+$/g, "");
			if (x == c_name.trim()) {
				y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
				return unescape(y);
			}
		}

		return "";
	};






})(

nilnul



);