﻿<%@ Control Language="C#" AutoEventWireup="true"  %>

time.local=function(numberUtc){
	var now = new Date(numberUtc);

	return new Date(Date.UTC(
		now.getFullYear(),
		now.getMonth(),
		now.getDate(),
		now.getHours(),
		now.getMinutes()
		,now.getSeconds()
	));


};
