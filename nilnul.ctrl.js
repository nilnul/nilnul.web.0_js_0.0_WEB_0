﻿/// <reference path="nilnul2.js" />


(function ( nilnul ) {
	nilnul.ctrl = {};

	var rnd = function (x, y) {
		return x + Math.random() * y;
	};



	rnd.int = function (x, y) {
		return Math.floor(rnd(x, y));
	};




	nilnul.getElementIds = function () {
		//get all elements ids

		var arr = [];
		var elem = document.getElementsByTagName('*'), i = 0, e;
		var id = "id";
		while (e = elem[i++]) {
			e[id] ? arr[arr.length] = e[id] : null;
		}
		return arr;

	};



	if (typeof String.prototype.startsWith != 'function') {
		String.prototype.startsWith = function (str) {
			return this.indexOf(str) == 0;
		};
	}


	if (typeof String.prototype.rndChar != 'function') {
		String.prototype.rndChar = function (str) {
			return s[randint(0, s.length)];
		}
	};

	//given a prefix, generate a string of certain lenght, loop, and append generated string if necessary.
	nilnul.ctrl.newId = (function () {
		var symbol = (function () {
			var gen = function (_symbol) {
				return 	{
					symbol: function (){ return _symbol; }
					,
					length: function (){ return _symbol.length; }
					,
					last: function (){ return _symbol[length - 1]; }
					,
					isLast: function (char){ return this.last() == char; }
					,
					first: function (){ return this.symbol()[0]; }
					,
					index: function (char){ return this.symbol().indexOf(char); }
					,
					rndChar: function () {
						return _symbol[rnd.int(0, _symbol.length)];
					}
				};
			};
			var prefix = gen("abcdefghijklmnopqrstuvwxyz");
			var suffix = gen("abcdefghijklmnopqrstuvwxyz_0123456789");
			var suffix2 = gen("0123456789abcdefghijklmnopqrstuvwxyz");
			var suffix3 = gen("abcdefghijklmnopqrstuvwxyz0123456789");

			return suffix2;
		})();

		var len =4;
		var prefix = "nn";

		var seg = (function () {
			var gen = function () {
				//len = len || _segLen;
				var r = "";
				while (r.length < len) {
					r = r + symbol.rndChar();
				}
				return r;
			};
			return gen;
		})();
		///everytime called.
		var appendFirstSeg = function () {
			return  prefix + seg(len);
		};

		var genPrefixAndFirstSeg = function () {
			return appendFirstSeg();
		};

		var crawl = function (curId, hurdle) {


			while (hurdle.toLowerCase().startsWith(curId.toLowerCase())) {
				curId = curId + seg();

			}
			return curId;
		}

		var crawlAll = function (curId, hurdles) {

			for (var i = 0; i < hurdles.length; i++) {

				curId = crawl(curId, hurdles[i]);
			}
			return curId;
		};


		var gen = function (_segLen, _prefix) {
			
			len =  _segLen||len;

			prefix =_prefix|| prefix;

			return crawlAll(genPrefixAndFirstSeg(), nilnul.getElementIds());
		};
		return gen;
	})();

	nilnul.rnd = rnd;

	return nilnul;

})(
	 nilnul
);
