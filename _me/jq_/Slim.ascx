﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%--
	The short answer taken from the announcement of jQuery 3.0 Final Release (https://blog.jquery.com/2016/06/09/jquery-3-0-final-released/) :


Along with the regular version of jQuery that includes the ajax and effects modules, we’re releasing a “slim” version that excludes these modules. All in all, it excludes ajax, effects, and currently deprecated code.

The file size (gzipped) is about 6k smaller, 23.6k vs 30k.

	--%>
