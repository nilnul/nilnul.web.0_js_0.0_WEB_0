﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<% #if false %>
<script>
	///<reference path="nilnul.measure.js" />
	(function (nilnul) {
		<% #endif %>

		;
		if (!String.prototype.trim) {
			String.prototype.trim = function () {
				$.trim;

				return this.replace(/^\s+|\s+$/g, '');

			};
		}


		nilnul.txt = {};

		nilnul.string = nilnul.str = nilnul.txt;

		var trim = function (str) {

			return $.trim(str);

		};

		nilnul.txt.trim = trim;
		var isNulOrWhite = function (s) {

			if (typeof (s) == "undefined") {
				return true;

			}

			if (s == null) {
				return true;

			}
			if (typeof s == "string" && $.trim(s) == "") {
				return true;

			}
			return false;

		};

		nilnul.txt.isNulOrWhite = isNulOrWhite;		//nul, not null, includes undefined. 
		nilnul.txt.nulable = function () { };
		nilnul.txt.nulable.be_ = function () { };
		nilnul.txt.nulable.be_.nulOrWhite = isNulOrWhite;
		nilnul.txt.nulable.be_.nul0blank = isNulOrWhite;

		nilnul.string.isNullOrWhiteSpace = isNulOrWhite;

		nilnul.string.notNullOrWhiteSpace = function (s) {
			return !nilnul.string.isNullOrWhiteSpace(s);

		};
		nilnul.txt.isNoNulOrWhite = function (s) {
			return !nilnul.txt.isNulOrWhite(s);

		};

		nilnul.txt.isNotNulOrWhite = nilnul.txt.isNoNulOrWhite;

		nilnul.string.repeat = function (s, n) {
			var r = "";
			while (n > 0) {
				r += s;
				n--;
			}
			return r;
		};


		nilnul.string.paddingBefore = function (str, padding, expectedLength) {

			var r = "";
			var paddingLength = expectedLength - str.length;
			while (r.length < paddingLength) {
				r += padding;
			}

			r = r.substr(0, paddingLength);
			return r + str;



		};

		nilnul.string.paddingAfter = function (str, padding, expectedLength) {

			var r = "";
			var paddingLength = expectedLength - str.length;
			while (r.length < paddingLength) {
				r += padding;
			}

			r = r.substr(0, paddingLength);
			return str + r;



		};
		nilnul.txt.eq_ = {};
		nilnul.txt.eq_.caseInsensitive = function (a, b) {
			return a.toUpperCase()() === b.toUpperCase();

		};



	<% #if false %>
	})(
		nilnul
	)
	;
</script>
<% #endif %>
