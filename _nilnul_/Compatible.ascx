﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="System.IO" %>

<% #if false %>
<script>
	(function (nilnul) {
		<% #endif %>
		;
		nilnul.appendEvents = function (target, evt, func) {
			var oldEvt = target[evt];
			if (typeof oldEvt != 'function') {
				target[evt] = func;
			} else {
				target[evt] = function (e) {
					oldEvt(e);
					func(e);
				}
			}
		};

		;nilnul.userLang = (window.navigator.language) ? window.navigator.language : window.navigator.userLanguage;

		;nilnul.getUserLang = function () {
			var s = window.nilnulEs.userLang;
			if (s.indexOf("cn") >= 0 || s.indexOf("zh") != -1) {
				return "cn";
			}
		}
		;nilnul.fireAction = function (action) {
			if (action && typeof (action) == "function") {
				action();
			}
		};

		;nilnul.isNullOrWhiteSpace = function (s) {
			if (typeof (s) == "undefined") {
				return true;
			}

			if (s == null) {
				return true;
			}
			if (typeof s == "string" && s.trim() == "") {
				return true;
			}
			return false;
		}

		;nilnul.isNoteNullOrWhiteSpace = function (s) {
			return !window.isNullOrWhiteSpace(s);
		}

		;nilnul.emailRegex = /^(?:[-_a-zA-Z0-9]+\.)*(?:[-_a-zA-Z0-9]+)@(?:[-_a-zA-Z0-9]+\.)+[-_a-zA-Z0-9]+$/;

		;nilnul.setCookie = function (c_name, value, exdays) {
			var exdate = new Date();
			exdate.setDate(exdate.getDate() + exdays);
			var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
			document.cookie = c_name + "=" + c_value;
		}

		;nilnul.getCookie = function (c_name) {
			var i, x, y, ARRcookies = document.cookie.split(";");
			for (i = 0; i < ARRcookies.length; i++) {
				x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
				x = x.trim();
				<%--//x.replace(/^\s+|\s+$/g, "");--%>
				if (x == c_name.trim) {
					y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
					return unescape(y);
				}
			}
		}

		;window.nilnulEs = window.nilnulJs = nilnul;
		;nilnul.userKeyInCookie = "nilnulUserKeyInCookie";

		;nilnul.queryString = function (key) {
			qs = window.location.search.substring(1);
			qa = qs.split("&");
			for (i = 0; i < qa.length; i++) {
				keyValue = qa[i].split("=");
				if (keyValue[0] == key) {
					return keyValue[1];
				}
			}
		}

		;nilnul.getHttp = function () {
			var xmlhttp = false;
			if (window.XMLHttpRequest) {
				xmlhttp = new XMLHttpRequest();
				if (xmlhttp.overrideMimeType) {
					xmlhttp.overrideMimeType('text/xml');
				}
			} else {
				try {
					xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
					try {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (E) {
						try {

							xmlhttp = new ActiveXObject("Msxml2.XMLHTTP.6.0");
						} catch (e2) {

							try { xmlhttp = new ActiveXObject("Msxml2.XMLHTTP.3.0"); }
							catch (e4) {
								xmlhttp = false;
							}
						}
					}
				}
			}
			return xmlhttp;
		}

		;nilnul.loadCss = function (cssPath) {
			if (document.createStyleSheet) { document.createStyleSheet(cssPath); }
			else {
				$("head").append($("<link rel='stylesheet' href='" + cssPath + "' type='text/css' media='screen' />"));
			}
		};
	<% #if false %>
	})(nilnul);
</script>
<% #endif %>