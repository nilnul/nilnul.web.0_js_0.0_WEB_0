﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="System.IO" %>

<% #if false %>
<script>
	///<reference path="nilnul.measure.js" />
	(function (nilnul) {
		<% #endif %>
		nilnul.content = {};
		nilnul.content.appendTo$ = function (content,$boxer) {
			var $loaded = $($.parseHTML(content.trim(), true)).filter(function (index, element) {<%--
	jQuery.parseHTML( data [, context ] [, keepScripts ] )

	Most jQuery APIs that accept HTML strings will run scripts that are included in the HTML. jQuery.parseHTML does not run scripts in the parsed HTML unless keepScripts is explicitly true. However, it is still possible in most environments to execute scripts indirectly, for example via the <img onerror> attribute. The caller should be aware of this and guard against it by cleaning or escaping any untrusted inputs from sources such as the URL or cookies. For future compatibility, callers should not depend on the ability to run any script content when keepScripts is unspecified or false.
				--%>
				//debugger;
				return element.nodeType === Node.ELEMENT_NODE;

			}).appendTo($boxer);

		};
		
	<% #if false %>
	})(
		nilnul
	)
	;
</script>
<% #endif %>