﻿<%@ Control Language="C#" AutoEventWireup="true" %>

<% #if false %>
<script>
	<% #endif %>
	; var nilnul = nilnul || {}
	; nilnul.obj = nilnul.obj || {}

	; nilnul.obj.addMethod = function(object, name, fn) {
		var old = object[name];
		object[name] = function () {
			if (fn.length === arguments.length) {
				return fn.apply(this, arguments);
			} else if (typeof old == 'function') {
				return old.apply(this, arguments);
			}
		}
	};
	<%--
		var ninjas = {
			values: ['Neal', 'yang', 'Nealyang', 'Neal yang']
		}

		addMethod(ninjas, 'find', function () {
			return this.values;
		});

		addMethod(ninjas, 'find', function (name) {
			var ret = [];
			for (var i = 0; i < this.values.length; i++) {
				if (this.values[i].indexOf(name) === 0) {
					ret.push(this.values[i]);
				}
			}
			return ret;
		});

		addMethod(ninjas, 'find', function (first, last) {
			var ret = [];
			for (var i = 0; i < this.values.length; i++) {
				if (this.values[i] == (first + ' ' + last))
					ret.push(this.values[i]);
			}
			return ret;
		});


		console.log(ninjas.find().length);
		console.log(ninjas.find('Neal'));
		console.log(ninjas.find('Neal', 'yang'));
	--%>
	<% #if false %>
</script>
<% #endif %>
