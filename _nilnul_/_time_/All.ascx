﻿<%@ Control Language="C#" AutoEventWireup="true" %>


<% #if false %>
<script>
	///<reference path="nilnul.measure.js" />
	(function (nilnul) {
		<% #endif %>
		nilnul.time = {};
		nilnul.time.local = function (numberUtc) {
			var now = new Date(numberUtc);

			return new Date(Date.UTC(
				now.getFullYear(),
				now.getMonth(),
				now.getDate(),
				now.getHours(),
				now.getMinutes()
				, now.getSeconds()
			));


		};


		nilnul.time.local.toUtc = function (numberLocal) {
			var now = new Date(numberLocal);
			return new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
		};



		<% #if false %>
	})(
		nilnul
	);
</script>
<% #endif %>

	
