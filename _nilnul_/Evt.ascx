﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<% #if false %>
<script>
	///<reference path="nilnul.measure.js" />
	(function (nilnul) {
		<% #endif %>
		;
		nilnul.event = {};

		nilnul.event.appendEvents = function (target, evt, func) {
			var oldEvt = target[evt];
			if (typeof oldEvt != 'function') {
				target[evt] = func;
			} else {
				target[evt] = function (e) {
					oldEvt(e);
					func(e);
				}
			}
		};

		nilnul.event.fireAction = function (action, data) {
			if (action && typeof (action) == "function") {
				action(data);
			}
		};

		nilnul.evt = nilnul.event;
		;
	<% #if false %>
	})(
		nilnul
	)
	;
</script>
<% #endif %>