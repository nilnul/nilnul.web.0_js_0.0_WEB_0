﻿// script included using test.js?a=10&z=50
function getLastChild(el) {
	return (el.lastChild && el.lastChild.nodeName != '#text') ? getLastChild(el.lastChild) : el;
}

var query = getLastChild(document.lastChild).getAttribute('src').replace(/.*\?/, '');




/*
Surely this will become a problem if you’re loading several scripts asynchronously? Example:
 - script tag 1 appended, starts downloading, this is now last child
 - script tag 2 appended, starts downloading, this is now last child
 - script tag 1 finished downloading, thinks it’s script tag 2

Please correct me if I’m wrong.



– yep, it’s just for those script tags that are loaded from the original markup

*/

