﻿///<reference path="nilnul.measure.js" />
(function (nilnul) {
    if (!String.prototype.trim) {
        String.prototype.trim = function () {
            $.trim;

            return this.replace(/^\s+|\s+$/g, '');

        };
    }

    nilnul.string = {};

    var isNulOrWhite = function (s) {

        if (typeof (s) == "undefined") {
            return true;

        }

        if (s == null) {
            return true;

        }
        if (typeof s == "string" && $.trim(s) == "") {
            return true;

        }
        return false;

    };
    nilnul.string.isNulOrWhite = isNulOrWhite;

    nilnul.string.isNullOrWhiteSpace = isNulOrWhite;

    nilnul.string.notNullOrWhiteSpace = function (s) {
        return !nilnul.string.isNullOrWhiteSpace(s);

    };

    nilnul.string.repeat = function (s, n) {
        var r = "";
        while (n > 0) {
            r += s;
            n--;
        }
        return r;
    };


    nilnul.string.paddingBefore = function (str, padding, expectedLength) {

        var r = "";
        var paddingLength = expectedLength - str.length;
        while (r.length < paddingLength) {
            r += padding;
        }

        r = r.substr(0, paddingLength);
        return r + str;



    };

    nilnul.string.paddingAfter = function (str, padding, expectedLength) {

        var r = "";
        var paddingLength = expectedLength - str.length;
        while (r.length < paddingLength) {
            r += padding;
        }

        r = r.substr(0, paddingLength);
        return str + r;



    };






})(

nilnul



);