﻿<p>
	Since the scripts are executed sequentially, the last script element will very often be the currently running script:
</p>
<pre><code>&lt;script&gt;
var scripts = document.getElementsByTagName( &#39;script&#39; );
var me = scripts[ scripts.length - 1 ];
&lt;/script&gt;
</code></pre>
<h3>Benefits</h3>
<ul>
	<li>Simple.</li>
	<li>Almost universally supported</li>
	<li>No custom attributes or id needed</li>
</ul>
<h3>Problems</h3>
<ul>
	<li>Does <strong>not</strong> work with asynchronous scripts (<code>defer</code> &amp; <code>async</code>)</li>
	<li>Does <strong>not</strong> work with scripts inserted dynamically</li>
</ul>
