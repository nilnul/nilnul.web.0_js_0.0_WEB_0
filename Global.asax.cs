﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace nilnul.web.js._web
{
	public class Global : System.Web.HttpApplication
	{

		protected void Application_Start(object sender, EventArgs e)
		{
			//// Code that runs on application startup
			//RouteConfig.RegisterRoutes(RouteTable.Routes);
			//BundleConfig.RegisterBundles(BundleTable.Bundles);

			//// Initialize the product database.
			//Database.SetInitializer(new ProductDatabaseInitializer());

			//// Create custom role and user.
			//RoleActions roleActions = new RoleActions();
			//roleActions.AddUserAndRole();

			// Add Routes.
			RegisterCustomRoutes(RouteTable.Routes);


		}

		static void RegisterCustomRoutes(RouteCollection routes)
		{
			routes.MapPageRoute(
				"ProductsByCategoryRoute",
				"Category/{categoryName}",
				"~/ProductList.aspx"
			);
			routes.MapPageRoute(
				"ProductByNameRoute",
				"Product/{productName}",
				"~/ProductDetails.aspx"
			);
		}


		protected void Session_Start(object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(object sender, EventArgs e)
		{

		}

		protected void Application_Error(object sender, EventArgs e)
		{

		}

		protected void Session_End(object sender, EventArgs e)
		{

		}

		protected void Application_End(object sender, EventArgs e)
		{

		}
	}
}