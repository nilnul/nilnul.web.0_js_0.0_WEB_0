﻿
(function (nilnulEs,$) {

	$.extend(nilnulEs,
	{
		loadCss: function (cssPath) {

			if (document.createStyleSheet) { document.createStyleSheet(cssPath); }
			else {
				$("head").append($("<link rel='stylesheet' href='"+cssPath+"' type='text/css' media='screen' />"));
			}

		}
	}
	);

})(
	nilnulEs,jQuery

);