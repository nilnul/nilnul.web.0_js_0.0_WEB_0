﻿(function (window, undefined) {

	window.nilnulEs = {

		createCssJs: function (filename, filetype) {
			if (filetype == "js") { //if filename is a external JavaScript file
				var fileref = document.createElement('script')
				fileref.setAttribute("type", "text/javascript")
				fileref.setAttribute("src", filename)
			}
			else if (filetype == "css") { //if filename is an external CSS file
				var fileref = document.createElement("link")
				fileref.setAttribute("rel", "stylesheet")
				fileref.setAttribute("type", "text/css")
				fileref.setAttribute("href", filename)
			}

			if (typeof fileref != "undefined") {
				document.getElementsByTagName("head")[0].appendChild(fileref);
			}


			
		},
		append: function (filename,filetype) {
			document.getElementsByTagName("head")[0].appendChild(nilnulEs.createCssJs(filename,filetype))

		}

	};



})(window);





window.onload = function () {
	var filename = "link.css", sheet, i;
	var fileref = document.createElement("link");

	fileref.setAttribute("rel", "stylesheet");
	fileref.setAttribute("type", "text/css");
	fileref.setAttribute("href", filename);

	readyfunc = function () {
		alert("File Loaded");
	}

	timerfunc = function () {
		for (i = 0; i < document.styleSheets.length; i++) {
			sheet = document.styleSheets[i].href;
			if (sheet !== null && sheet.substr(sheet.length - filename.length) == filename)
				return readyfunc();
		}
		setTimeout(timerfunc, 50);
	}

	if (document.all) { //Uses onreadystatechange for Internet Explorer
		fileref.attachEvent('onreadystatechange', function () {
			if (fileref.readyState == 'complete' || fileref.readyState == 'loaded')
				readyfunc();
		});
	} else {    //Checks if the stylesheet has been loaded every 50 ms for others
		setTimeout(timerfunc, 50);
	}
	document.getElementsByTagName("head")[0].appendChild(fileref);
}



$(document).ready(function () {

	if ($("#container").size() > 0) {
		if (document.createStyleSheet) {
			document.createStyleSheet('style.css');
		}
		else {
			$("head").append($("<link rel='stylesheet' href='style.css' type='text/css' media='screen' />"));
		}
	}
});

