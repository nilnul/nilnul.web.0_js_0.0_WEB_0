﻿

(
	function (window, undefined) {


	window.nilnulJs = {
		trim: String.prototype.trim,
		trim_regex: function (s){ return s.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); },
		emailRegex: /^(?:[-_a-zA-Z0-9]+\.)*(?:[-_a-zA-Z0-9]+)@(?:[-_a-zA-Z0-9]+\.)+[-_a-zA-Z0-9]+$/


	};

	window.nilnulEs = window.nilnulJs;


	window.nilnulJs.appendEvents = function (target, evt, func) {

		var oldEvt = target[evt];
		if (typeof oldEvt != 'function') {
			target[evt] = func;
		} else {
			target[evt] = function (e) {
				oldEvt(e);
				func(e);
			}
		}
	};



	window.nilnulEs.isEmail = function (s) {
		return window.nilnulEs.emailRegex.test(s);

	};

	window.nilnulEs.userLang = (window.navigator.language) ? window.navigator.language : window.navigator.userLanguage;



	window.nilnulEs.getUserLang = function () {
		var s = window.nilnulEs.userLang;
		if (s.indexOf("cn") >= 0 || s.indexOf("zh") != -1) {
			return "cn";

		}
	}
	window.nilnulEs.fireAction = function (action) {
		if (action && typeof(action) == "function") {
			action();
		}
	};

	window.nilnulEs.loadControl = function () {


	}

	//	nilnulJs.trim = String.prototype.trim;

	//nilnulJs.trim_regex = function (){ return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); };

	//window.nilnulJs.isNullOrWhiteSpace = function (s) {
	//	if (s == null) {
	//		return true;

	//	}
	//	if (typeof(s) == "undefined") {
	//		return null;

	//	}
	//	if (s.trim() == "") {
	//		return true;

	//	}
	//	return false;


	//}

	window.nilnulJs.isNullOrWhiteSpace = function (s) {
		if (typeof(s) == "undefined") {
			return true;

		}

		if (s == null) {
			return true;

		}
		if (typeof s == "string" && s.trim() == "") {
			return true;

		}
		return false;


	}


	window.nilnulJs.isNoteNullOrWhiteSpace = function (s) {
		return !window.isNullOrWhiteSpace(s);

	}

	window.nilnulJs.emailRegex = /^(?:[-_a-zA-Z0-9]+\.)*(?:[-_a-zA-Z0-9]+)@(?:[-_a-zA-Z0-9]+\.)+[-_a-zA-Z0-9]+$/;

	window.nilnulJs.setCookie = function (c_name, value, exdays) {
		var exdate = new Date();
		exdate.setDate(exdate.getDate() + exdays);

		var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
		document.cookie = c_name + "=" + c_value;
	}

	window.nilnulJs.getCookie = function (c_name) {
		var i, x, y, ARRcookies = document.cookie.split(";");

		for (i = 0; i < ARRcookies.length; i++) {
			x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));

			x = x.trim();
			//x.replace(/^\s+|\s+$/g, "");
			if (x == c_name.trim) {
				y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
				return unescape(y);
			}
		}
	}

	window.nilnulJs.userKeyInCookie = "nilnulUserKeyInCookie";

	window.nilnulJs.getUser = function () {
		return getCookie(window.userKeyInCookie);
	}

	window.nilnulJs.queryString = function (key) {
		qs = window.location.search.substring(1);

		qa = qs.split("&");

		for (i = 0; i < qa.length; i++) {
			keyValue = qa[i].split("=");
			if (keyValue[0] == key) {
				return keyValue[1];
			}
		}
	}

	window.nilnulEs.getHttp = function () {
		var xmlhttp = false;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
			if (xmlhttp.overrideMimeType) {
				xmlhttp.overrideMimeType('text/xml');
			}
		}else {
			try {
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (E) {
					try {

						xmlhttp = new ActiveXObject("Msxml2.XMLHTTP.6.0");





					} catch (e2) {

						try { xmlhttp = new ActiveXObject("Msxml2.XMLHTTP.3.0"); }
						catch (e4) {
							xmlhttp = false;
						}
					}
				}
			}
		}
		return xmlhttp;
	}


	nilnulEs.loadCss = function (cssPath) {

		if (document.createStyleSheet) { document.createStyleSheet(cssPath); }
		else {
			$("head").append($("<link rel='stylesheet' href='" + cssPath + "' type='text/css' media='screen' />"));
		}

	};



}

)
(
	window
);

